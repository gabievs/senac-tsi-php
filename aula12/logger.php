<?php
/*
Função para gravar logs do sistema
*/
/* Exemplo de função simples
function logger(string $str): bool {
	$fp = fopen('log.txt', 'a');
	if ( fwrite($fp, $str) ){
		fclose($fp);
		return true;
	} else {
		fclose($fp);
		return false;
	}
}
*/
/* Exemplo com mais de um parâmetro */
function logger(string $str, int $nr_linha): bool {
	$fp = fopen('log.txt', 'a');
	if ( fwrite($fp, $nr_linha . ': ' . $str) ){
		fclose($fp);
		return true;
	} else {
		fclose($fp);
		return false;
	}
}
/**/

/* Exemplo com parâmetro não obrigatório 
function logger(string $str, int $nr_linha = null): bool {
	$fp = fopen('log.txt', 'a');
	if ( fwrite($fp, $nr_linha . ': ' . $str) ){
		fclose($fp);
		return true;
	} else {
		fclose($fp);
		return false;
	}
}
*/





