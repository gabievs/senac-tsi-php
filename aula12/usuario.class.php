<?php


class Usuario {

private $id;
private $nome;
private $email; 
private $senha;
private $objDb;

// O Construtor é obrigatoriamente public
public function __construct(){

    $this->objDb = new mysqli(	'localhost',
                        'root',
                        '',
                        'aula_php',
                        3307);

    // $servidor = "localhost";
    // $usuario = "root";
    // $senha = "";
    // $SGBD = "aula12_php";
    // $conexao = mysqli_connect($servidor, $usuario, $senha, $SGBD);
}


// SETTERS
// Um Set seta o valor para a variável ou método
public function setId (int $id) {
    $this->id = $id;
}

public function setNome (string $nome) {
    $this->nome = $nome;
}

public function setEmail (string $email) {
    $this->email = $email;
}

public function setSenha (string $senha) {
    $this->senha = password_hash($senha, PASSWORD_DEFAULT);

}

// GETTERS
// O Get pega o valor da classe
public function getId (int $id) : int {
   return $this->id = $id;
}

public function getNome (string $nome) : string {
    return $this->nome = $nome;
}

public function getEmail (string $email) : string {
    return $this->email = $email;
}

public function getSenha (string $senha) : string {
    return $this->senha = $senha;
}


//Funções

public function listarUser(){
    $objStmt = $this->objDb->prepare('SELECT * FROM usuario WHERE id = ?');
    
    $objStmt->bind_param('i', $this->id);

    if($objStmt->execute()){
        return true;
    }else{
        return false;
    }

}

public function deleteUser(){
    $objStmt = $this->objDb->prepare('DELETE FROM login WHERE id = ?');

    $objStmt->bind_param('i', $this->id);

    if($objStmt->execute()){
        return true;
    }else{
        return false;
    }
}

public function saveUser(){
    $objStmt = $this->objDb->prepare('REPLACE INTO login (id, nome, email, senha) VALUES (?, ?, ?, ?)');

    $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);
    
    if($objStmt->execute()) {
        return true;
    }else{
        return false;
    }
}


public function __destruct() {
    echo "<br> Fechando a conexão com o SGDB Teste";
}

}


